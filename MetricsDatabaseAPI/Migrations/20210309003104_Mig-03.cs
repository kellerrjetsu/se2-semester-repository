﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MetricsDatabaseAPI.Migrations
{
    public partial class Mig03 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SalesLogs");

            migrationBuilder.DropTable(
                name: "SalesMetrics");

            migrationBuilder.DropTable(
                name: "Visitors");

            migrationBuilder.CreateTable(
                name: "Infrastructures",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TimeStamp = table.Column<DateTime>(nullable: false),
                    CPUUtilization = table.Column<double>(nullable: false),
                    FreeMemory = table.Column<double>(nullable: false),
                    FreeStorageSpace = table.Column<double>(nullable: false),
                    NetworkReceive = table.Column<double>(nullable: false),
                    NetworkTransmit = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Infrastructures", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Throughputs",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TimeStamp = table.Column<DateTime>(nullable: false),
                    DBConnections = table.Column<double>(nullable: false),
                    WriteIOPS = table.Column<double>(nullable: false),
                    ReadIOPS = table.Column<double>(nullable: false),
                    WriteThroughput = table.Column<double>(nullable: false),
                    ReadThroughput = table.Column<double>(nullable: false),
                    WriteLatency = table.Column<double>(nullable: false),
                    ReadLatency = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Throughputs", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Infrastructures");

            migrationBuilder.DropTable(
                name: "Throughputs");

            migrationBuilder.CreateTable(
                name: "SalesLogs",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    NumberItems = table.Column<int>(type: "int", nullable: false),
                    TotalSale = table.Column<double>(type: "float", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SalesLogs", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SalesMetrics",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Metric1 = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Metric2 = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SalesMetrics", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Visitors",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    HasCart = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    MakesSale = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Visitors", x => x.Id);
                });
        }
    }
}
