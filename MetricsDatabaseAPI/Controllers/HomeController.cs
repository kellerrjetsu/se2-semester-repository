﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MetricsDatabaseAPI.Models;
using MetricsDatabaseAPI.Services;

namespace MetricsDatabaseAPI.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IThroughputsRepository _dbThroughput;

        public HomeController(ILogger<HomeController> logger, IThroughputsRepository dbVisitors)
        {
            _logger = logger;
            _dbThroughput = dbVisitors;
        }

        public IActionResult Index()
        {
            var model = _dbThroughput.ReadAllThroughputs();
            return View(model);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
