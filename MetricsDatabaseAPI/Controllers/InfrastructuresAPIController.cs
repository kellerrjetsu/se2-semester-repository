﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using MetricsDatabaseAPI.Models.Entities;
using MetricsDatabaseAPI.Models.ViewModels;
using MetricsDatabaseAPI.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace MetricsDatabaseAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class InfrastructuresAPIController : Controller
    {
        private IInfrastructuresRepository _infrastructuresRepo;

        public InfrastructuresAPIController(IInfrastructuresRepository infrastructuresRepository)
        {
            _infrastructuresRepo = infrastructuresRepository;
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromForm] int Id)
        {
            var allInfrastructures = _infrastructuresRepo.ReadAllInfrastructures();
            var infrastructure = allInfrastructures.ElementAt(Id);
            if (infrastructure == null)
            {
                return NotFound();
            }
            return Ok(infrastructure);
        }

        [HttpPost("create")]
        public IActionResult Post([FromForm] CreateInfrastructureVM infrastructureVM)
        {
            var infrastructure = infrastructureVM.CreateInfrastructure();
            _infrastructuresRepo.CreateInfrastructure(infrastructure);
            return CreatedAtAction("Get", new { id = infrastructure.Id }, infrastructure);
        }

        [HttpPut("update")]
        public IActionResult Put([FromForm] Infrastructure infrastructure)
        {
            _infrastructuresRepo.UpdateInfrastructure(infrastructure.Id, infrastructure);
            return NoContent(); // 204 as per HTTP specification
        }

        [HttpDelete("delete")]
        public IActionResult Delete([FromForm] int Id)
        {
            _infrastructuresRepo.DeleteInfrastructure(Id);
            return NoContent();
        }
    }
}
