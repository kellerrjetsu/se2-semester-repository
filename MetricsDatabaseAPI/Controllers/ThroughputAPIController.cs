﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using MetricsDatabaseAPI.Models.Entities;
using MetricsDatabaseAPI.Models.ViewModels;
using MetricsDatabaseAPI.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace MetricsDatabaseAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ThroughputAPIController : Controller
    {
        private IThroughputsRepository _throughputsRepo;
        private static readonly Random _random = new Random();

        public ThroughputAPIController(IThroughputsRepository throughputsRepository)
        {
            _throughputsRepo = throughputsRepository;
        }

        [HttpGet("random")]
        public async Task<IActionResult> Get()
        {
            var allThroughputs = await _throughputsRepo.ReadAllThroughputsAsync();
            var count = allThroughputs.Count();
            var index = _random.Next(0, count);
            var throughput = allThroughputs.ElementAt(index);
            return Ok(new { throughput.Id, throughput.TimeStamp, throughput.DBConnections, throughput.WriteIOPS, throughput.ReadIOPS, throughput.WriteThroughput, throughput.ReadThroughput, throughput.WriteLatency, throughput.ReadLatency });
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromForm] int Id)
        {
            var allThroughputs = _throughputsRepo.ReadAllThroughputs();
            var throughput = allThroughputs.ElementAt(Id);
            if (throughput == null)
            {
                return NotFound();
            }
            return Ok(throughput);
        }

        [HttpPost("create")]
        public IActionResult Post([FromForm] CreateThroughputVM throughputVM)
        {
            var throughput = throughputVM.CreateThroughput();
            _throughputsRepo.CreateThroughput(throughput);
            return CreatedAtAction("Get", new { id = throughput.Id }, throughput);
        }

        [HttpPut("update")]
        public IActionResult Put([FromForm] Throughput throughput)
        {
            _throughputsRepo.UpdateThroughput(throughput.Id, throughput);
            return NoContent(); // 204 as per HTTP specification
        }

        [HttpDelete("delete")]
        public IActionResult Delete([FromForm] int Id)
        {
            _throughputsRepo.DeleteThroughput(Id);
            return NoContent();
        }
    }
}
