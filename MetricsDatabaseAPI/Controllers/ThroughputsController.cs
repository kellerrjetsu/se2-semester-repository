﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MetricsDatabaseAPI.Services;

namespace MetricsDatabaseAPI.Controllers
{
    public class ThroughputsController : Controller
    {
        private readonly IThroughputsRepository _dbThroughputs;

        public ThroughputsController(IThroughputsRepository dbThroughputs)
        {
            _dbThroughputs = dbThroughputs;
        }
        public IActionResult Index()
        {
            return View();
        }
    }
}
