﻿'use strict';
$(document).ready(function _homeIndex() {
    $('#timeStamp').html("Getting first throughput...");
    // Get the first Throughput
    _updatePageWithThroughput();

    // Update the Throughput every 10 seconds using a timer
    var timerHandle = setInterval(function _getThroughput10Secs() {
        _updatePageWithThroughput();
    }, 10000);

    // Clean up the interval timer when we navigate from the page
    $(window).on('unload', function _clearThroughputTimer() {
        clearInterval(timerHandle);
    });

    $("#createArea").hide();

    function _updatePageWithThroughput() {
        $.ajax({
            url: "/api/throughputapi/random/",
            success: function _ajaxSuccess(response) {
                let throughputInfo = response;
                $("#editThroughputBtn").attr("data-id", throughputInfo.id);
                $("#deleteThroughputBtn").attr("data-id", throughputInfo.id);
                $("#showThroughputBtn").attr("data-id", throughputInfo.id);
                console.log($('throughputInfo'));
                $("#timeStamp").fadeOut(250, function _fadeInThroughput() {
                    // Insert the Throughput into #theThroughput
                    $("#timeStamp").html(`"${throughputInfo.timeStamp}"`);
                    // Fade in #theThroughput over 250 ms
                    $("#timeStamp").fadeIn(250);
                });

                $("#dbConnections").fadeOut(250, function _fadeInThroughput() {
                    // Insert the Throughput into #theThroughput
                    $("#dbConnections").html(`"${throughputInfo.dbConnections}"`);
                    // Fade in #theThroughput over 250 ms
                    $("#dbConnections").fadeIn(250);
                });

                $("#writeIOPS").fadeOut(250, function _fadeInThroughput() {
                    // Insert the Throughput into #theThroughput
                    $("#writeIOPS").html(`"${throughputInfo.writeIOPS}"`);
                    // Fade in #theThroughput over 250 ms
                    $("#writeIOPS").fadeIn(250);
                });

                $("#readIOPS").fadeOut(250, function _fadeInThroughput() {
                    // Insert the Throughput into #theThroughput
                    $("#readIOPS").html(`"${throughputInfo.readIOPS}"`);
                    // Fade in #theThroughput over 250 ms
                    $("#readIOPS").fadeIn(250);
                });

                $("#writeThroughput").fadeOut(250, function _fadeInThroughput() {
                    // Insert the Throughput into #theThroughput
                    $("#writeThroughput").html(`"${throughputInfo.writeThroughput}"`);
                    // Fade in #theThroughput over 250 ms
                    $("#writeThroughput").fadeIn(250);
                });

                $("#readThroughput").fadeOut(250, function _fadeInThroughput() {
                    // Insert the Throughput into #theThroughput
                    $("#readThroughput").html(`"${throughputInfo.readThroughput}"`);
                    // Fade in #theThroughput over 250 ms
                    $("#readThroughput").fadeIn(250);
                });

                $("#writeLatency").fadeOut(250, function _fadeInThroughput() {
                    // Insert the Throughput into #theThroughput
                    $("#writeLatency").html(`"${throughputInfo.writeLatency}"`);
                    // Fade in #theThroughput over 250 ms
                    $("#writeLatency").fadeIn(250);
                });

                $("#readLatency").fadeOut(250, function _fadeInThroughput() {
                    // Insert the Throughput into #theThroughput
                    $("#readLatency").html(`"${throughputInfo.readLatency}"`);
                    // Fade in #theThroughput over 250 ms
                    $("#readLatency").fadeIn(250);
                });
            },
            error: function _ajaxError() {
                $("#timeStamp").html("Sorry, an application error occurred!");
            }
        }); // End ajax
    }
    
    $(document).on("submit", "#throughputForm", function _onCreateThroughputSubmit(event) {
        event.preventDefault();
        let $form = $(this);
        console.log($form);
        $.ajax({
            url: $form.attr("action"),
            type: $form.attr("method"),
            data: $form.serialize(),
            success: function _ajaxSuccess(response) {
                $('#createArea').hide(1000, () => {
                    $('#createThroughputBtn').show();
                    $('#editThroughputBtn').show();
                    $('#deleteThroughputBtn').show();
                })
            },
            error: function _ajaxError() {
                console.log("Could not create the Throughput");
            }
        });
    });

    $(document).on("submit", "#editThroughputForm", function _onEditThroughputSubmit(event) {
        event.preventDefault();
        let $form = $(this);
        console.log($form);
        $.ajax({
            url: $form.attr("action"),
            type: $form.attr("method"),
            data: $form.serialize(),
            success: function _ajaxSuccess(response) {
                $('#editArea').hide(1000, () => {
                    $('#createThroughputBtn').show();
                    $('#editThroughputBtn').show();
                    $('#deleteThroughputBtn').show();
                })
            },
            error: function _ajaxError() {
                console.log("Could not edit the Throughput");
            }
        });
    });

    $(document).on("submit", "#deleteThroughputForm", function _onDeleteThroughputSubmit(event) {
        event.preventDefault();
        let $form = $(this);
        console.log($form);
        $.ajax({
            url: $form.attr("action"),
            type: $form.attr("method"),
            data: $form.serialize(),
            success: function _ajaxSuccess(response) {
                $('#deleteArea').hide(1000, () => {
                    console.log("did delete happen?");
                    $('#createThroughputBtn').show();
                    $('#editThroughputBtn').show();
                    $('#deleteThroughputBtn').show();
                })
            },
            error: function _ajaxError() {
                console.log("Could not delete the Throughput");
            }
        });
    });


    $("#createThroughputBtn").on('click', function _showCreateThroughputForm(event) {
        $('#createArea').show(1000, () => {
            $('#createThroughputBtn').hide();
            $('#editThroughputBtn').hide();
            $('#deleteThroughputBtn').hide();
        });
        event.preventDefault();
    });

    $("#editThroughputBtn").on('click', function _showEditThroughputForm(event) {
        $('#editArea').show(1000, () => {
            console.log($('#editThroughputBtn').attr('data-id'));
            //console.log($('#id').attr('data-id'));
            console.log($('#HasCart').html().replace(/"/g,''));
            console.log($('#MakesSale').html().replace(/"/g, ''));
            $('#editThroughputForm #Id').val($('#editThroughputBtn').attr('data-id'));
            $('#editThroughputForm #TimeStamp').val($('#TimeStamp').html().replace(/"/g, ''));
            $('#editThroughputForm #DBConnections').val($('#DBConnections').html().replace(/"/g, ''));
            $('#editThroughputForm #WriteIOPS').val($('#WriteIOPS').html().replace(/"/g, ''));
            $('#editThroughputForm #ReadIOPS').val($('#ReadIOPS').html().replace(/"/g, ''));
            $('#editThroughputForm #WriteThroughput').val($('#WriteThroughput').html().replace(/"/g, ''));
            $('#editThroughputForm #ReadThroughput').val($('#ReadThroughput').html().replace(/"/g, ''));
            $('#editThroughputForm #WriteLatency').val($('#WriteLatency').html().replace(/"/g, ''));
            $('#editThroughputForm #ReadLatency').val($('#ReadLatency').html().replace(/"/g, ''));
            $('#createThroughputBtn').hide();
            $('#editThroughputBtn').hide();
            $('#deleteThroughputBtn').hide();
        });
        event.preventDefault();
    });

    $("#deleteThroughputBtn").on('click', function _showDeleteThroughputForm(event) {
        $('#deleteArea').show(1000, () => {
            console.log($('#deleteThroughputBtn').attr('data-id'));
            console.log($('#HasCart').html().replace(/"/g, ''));
            console.log($('#whoSaidIt').html().replace(/"/g, ''));
            $('#deleteThroughputForm #Id').val($('#deleteThroughputBtn').attr('data-id'));
            $('#editThroughputForm #TimeStamp').val($('#TimeStamp').html().replace(/"/g, ''));
            $('#editThroughputForm #DBConnections').val($('#DBConnections').html().replace(/"/g, ''));
            $('#editThroughputForm #WriteIOPS').val($('#WriteIOPS').html().replace(/"/g, ''));
            $('#editThroughputForm #ReadIOPS').val($('#ReadIOPS').html().replace(/"/g, ''));
            $('#editThroughputForm #WriteThroughput').val($('#WriteThroughput').html().replace(/"/g, ''));
            $('#editThroughputForm #ReadThroughput').val($('#ReadThroughput').html().replace(/"/g, ''));
            $('#editThroughputForm #WriteLatency').val($('#WriteLatency').html().replace(/"/g, ''));
            $('#editThroughputForm #ReadLatency').val($('#ReadLatency').html().replace(/"/g, ''));
            $('#createThroughputBtn').hide();
            $('#editThroughputBtn').hide();
            $('#deleteThroughputBtn').hide();
        });
        event.preventDefault();
    });

    $("#showThroughputBtn").on('click', function _showShowThroughputForm(event) {
        $('#showArea').show(1000, () => {
            console.log($('#showThroughputBtn').attr('data-id'));
            console.log($('#TimeStamp').html().replace(/"/g, ''));
            console.log($('#DBConnections').html().replace(/"/g, ''));
            console.log($('#WriteIOPS').html().replace(/"/g, ''));
            console.log($('#ReadIOPS').html().replace(/"/g, ''));
            console.log($('#WriteThroughput').html().replace(/"/g, ''));
            console.log($('#ReadThroughput').html().replace(/"/g, ''));
            console.log($('#WriteLatency').html().replace(/"/g, ''));
            console.log($('#ReadLatency').html().replace(/"/g, ''));
            $('#createThroughputBtn').hide();
            $('#editThroughputBtn').hide();
            $('#deleteThroughputBtn').hide();
            $.ajax({
                url: "api/throughputsapi/3",
                success: function _ajaxSuccess(response) {
                    console.log(response);
                },
                error: function _ajaxError() {
                    console.log("An error occourred!");
                }
            });
        });
        event.preventDefault();
    });

    $('#cancelBtn').on('click', function _hideCreateThroughputForm(event) {
        $('#createArea').hide(1000, () => {
            $('#createThroughputBtn').show();
            $('#editThroughputBtn').show();
            $('#deleteThroughputBtn').show();
        });
        event.preventDefault();
    });

    $('#cancelEditBtn').on('click', function _hideEditThroughputForm(event) {
        $('#editArea').hide(1000, () => {
            $('#createThroughputBtn').show();
            $('#editThroughputBtn').show();
            $('#deleteThroughputBtn').show();
        });
        event.preventDefault();
    });

    $('#cancelDeleteBtn').on('click', function _hideDeleteThroughputForm(event) {
        $('#deleteArea').hide(1000, () => {
            $('#createThroughputBtn').show();
            $('#editThroughputBtn').show();
            $('#deleteThroughputBtn').show();
        });
        event.preventDefault();
    }); 
});
