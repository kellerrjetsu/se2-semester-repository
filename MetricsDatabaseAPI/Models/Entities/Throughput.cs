﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MetricsDatabaseAPI.Models.Entities
{
    public class Throughput
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public DateTime TimeStamp { get; set; }
        public double DBConnections { get; set; }
        public double WriteIOPS { get; set; }
        public double ReadIOPS { get; set; }
        public double WriteThroughput { get; set; }
        public double ReadThroughput { get; set; }
        public double WriteLatency { get; set; }
        public double ReadLatency { get; set; }

        public Throughput CreateThroughput()
        {
            return new Throughput
            {
                Id = 0,
                TimeStamp = DateTime.Now,
                DBConnections = this.DBConnections,
                WriteIOPS = this.WriteIOPS,
                ReadIOPS = this.ReadIOPS,
                WriteThroughput = this.WriteThroughput,
                ReadThroughput = this.ReadThroughput,
                WriteLatency = this.WriteLatency,
                ReadLatency = this.ReadLatency
            };
        }
    }
}
