﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MetricsDatabaseAPI.Models.Entities
{
    public class Infrastructure
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public DateTime TimeStamp { get; set; }
        public double CPUUtilization { get; set; }
        public double FreeMemory { get; set; }
        public double FreeStorageSpace { get; set; }
        public double NetworkReceive { get; set; }
        public double NetworkTransmit { get; set; }

        public Infrastructure CreateInfrastructure()
        {
            return new Infrastructure
            {
                Id = 0,
                TimeStamp = DateTime.Now,
                CPUUtilization = this.CPUUtilization,
                FreeMemory = this.FreeMemory,
                FreeStorageSpace = this.FreeStorageSpace,
                NetworkReceive = this.NetworkReceive,
                NetworkTransmit = this.NetworkTransmit
            };
        }
    }

}
