﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MetricsDatabaseAPI.Models.Entities;

namespace MetricsDatabaseAPI.Models.ViewModels
{
    public class CreateInfrastructureVM
    {
        public double CPUUtilization { get; set; }
        public double FreeMemory { get; set; }
        public double FreeStorageSpace { get; set; }
        public double NetworkReceive { get; set; }
        public double NetworkTransmit { get; set; }

        public Infrastructure CreateInfrastructure()
        {
            return new Infrastructure
            {
                Id = 0,
                TimeStamp = DateTime.Now,
                CPUUtilization = this.CPUUtilization,
                FreeMemory = this.FreeMemory,
                FreeStorageSpace = this.FreeStorageSpace,
                NetworkReceive = this.NetworkReceive,
                NetworkTransmit = this.NetworkTransmit
            };
        }
    }
}
