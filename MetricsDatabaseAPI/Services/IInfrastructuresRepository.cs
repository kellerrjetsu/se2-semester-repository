﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MetricsDatabaseAPI.Models.Entities;

namespace MetricsDatabaseAPI.Services
{
    public interface IInfrastructuresRepository
    {
        Infrastructure ReadInfrastructure(int Id);
        Task<ICollection<Infrastructure>> ReadAllInfrastructuresAsync();
        ICollection<Infrastructure> ReadAllInfrastructures();
        Infrastructure CreateInfrastructure(Infrastructure infrastructure);
        void DeleteInfrastructure(int Id);
        void UpdateInfrastructure(int Id, Infrastructure infrastructure);
    }
}
