﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using MetricsDatabaseAPI.Models.Entities;

namespace MetricsDatabaseAPI.Services
{
    public class MetricsDbContext : DbContext
    {
        public MetricsDbContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Infrastructure> Infrastructures { get; set; }
        public DbSet<Throughput> Throughputs { get; set; }
    }
}
