﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MetricsDatabaseAPI.Models.Entities;

namespace MetricsDatabaseAPI.Services
{
    public class DBThroughputsRepository : IThroughputsRepository
    {
        private readonly MetricsDbContext _db;

        public DBThroughputsRepository(MetricsDbContext db)
        {
            _db = db;
        }

        public Throughput ReadThroughput(int id)
        {
            return _db.Throughputs.FirstOrDefault(i => i.Id == id);
        }

        public async Task<ICollection<Throughput>> ReadAllThroughputsAsync()
        {
            return await _db.Throughputs.ToListAsync();
        }

        public ICollection<Throughput> ReadAllThroughputs()
        {
            return _db.Throughputs.ToList();
        }

        public Throughput CreateThroughput(Throughput throughput)
        {
            _db.Throughputs.Add(throughput);
            _db.SaveChanges();
            return throughput;
        }

        public void DeleteThroughput(int throughputId)
        {
            var item = ReadThroughput(throughputId);
            if (item != null)
            {
                _db.Throughputs.Remove(item);
                _db.SaveChanges();
            }
        }

        public void UpdateThroughput(int throughputId, Throughput throughput)
        {
            var throughputToUpdate = ReadThroughput(throughputId);
            if (throughputToUpdate != null)
            {
                throughputToUpdate.TimeStamp = throughput.TimeStamp;
                throughputToUpdate.DBConnections = throughput.DBConnections;
                throughputToUpdate.WriteIOPS = throughput.WriteIOPS;
                throughputToUpdate.ReadIOPS = throughput.ReadIOPS;
                throughputToUpdate.WriteThroughput = throughput.WriteThroughput;
                throughputToUpdate.ReadThroughput = throughput.ReadThroughput;
                throughputToUpdate.WriteLatency = throughput.WriteLatency;
                throughputToUpdate.ReadLatency = throughput.ReadLatency;
                _db.SaveChanges();
            }
        }
    }
}
