﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MetricsDatabaseAPI.Models.Entities;

namespace MetricsDatabaseAPI.Services
{
    public class DBInfrastructuresRepository : IInfrastructuresRepository
    {
        private readonly MetricsDbContext _db;

        public DBInfrastructuresRepository(MetricsDbContext db)
        {
            _db = db;
        }

        public Infrastructure ReadInfrastructure(int id)
        {
            return _db.Infrastructures.FirstOrDefault(i => i.Id == id);
        }

        public async Task<ICollection<Infrastructure>> ReadAllInfrastructuresAsync()
        {
            return await _db.Infrastructures.ToListAsync();
        }

        public ICollection<Infrastructure> ReadAllInfrastructures()
        {
            return _db.Infrastructures.ToList();
        }

        public Infrastructure CreateInfrastructure(Infrastructure infrastructure)
        {
            _db.Infrastructures.Add(infrastructure);
            _db.SaveChanges();
            return infrastructure;
        }

        public void DeleteInfrastructure(int infrastructureId)
        {
            var item = ReadInfrastructure(infrastructureId);
            if (item != null)
            {
                _db.Infrastructures.Remove(item);
                _db.SaveChanges();
            }
        }

        public void UpdateInfrastructure(int infrastructureId, Infrastructure infrastructure)
        {
            var infrastructureToUpdate = ReadInfrastructure(infrastructureId);
            if (infrastructureToUpdate != null)
            {
                infrastructureToUpdate.TimeStamp = infrastructure.TimeStamp;
                infrastructureToUpdate.CPUUtilization = infrastructure.CPUUtilization;
                infrastructureToUpdate.FreeMemory = infrastructure.FreeMemory;
                infrastructureToUpdate.FreeStorageSpace = infrastructure.FreeStorageSpace;
                infrastructureToUpdate.NetworkReceive = infrastructure.NetworkReceive;
                infrastructureToUpdate.NetworkTransmit = infrastructure.NetworkTransmit;
                _db.SaveChanges();
            }
        }
    }
}
