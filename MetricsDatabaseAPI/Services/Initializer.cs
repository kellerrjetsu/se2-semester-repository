﻿using MetricsDatabaseAPI.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MetricsDatabaseAPI.Services
{
   public class Initializer
   {
      private MetricsDbContext _db;

      public Initializer(MetricsDbContext db)
      {
         _db = db;
      }

      public async Task SeedQuotesAsync()
      {
         await _db.Database.EnsureCreatedAsync();

         if (_db.Throughputs.Any()) return; // Already seeded

         var throughputs = new List<Throughput>
         {
            new Throughput
            {
               TimeStamp = DateTime.Now.AddDays(-10),
               DBConnections = 5,
               WriteIOPS = 3,
               ReadIOPS = 2,
               WriteThroughput = .02,
               ReadThroughput = .01,
               WriteLatency = 5,
               ReadLatency = 3
            },
            new Throughput
            {
               TimeStamp = DateTime.Now.AddDays(-50),
               DBConnections = 5,
               WriteIOPS = 5,
               ReadIOPS = 3,
               WriteThroughput = .05,
               ReadThroughput = .04,
               WriteLatency = 7,
               ReadLatency = 5
            },
            new Throughput
            {
               TimeStamp = DateTime.Now.AddDays(-20),
               DBConnections = 3,
               WriteIOPS = 1,
               ReadIOPS = 0,
               WriteThroughput = .002,
               ReadThroughput = .001,
               WriteLatency = 3,
               ReadLatency = 2
            }
         };
         await _db.Throughputs.AddRangeAsync(throughputs);
         _db.SaveChanges();
      }
   }
}
