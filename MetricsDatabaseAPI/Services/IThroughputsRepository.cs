﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MetricsDatabaseAPI.Models.Entities;

namespace MetricsDatabaseAPI.Services
{
    public interface IThroughputsRepository
    {
        Throughput ReadThroughput(int Id);
        Task<ICollection<Throughput>> ReadAllThroughputsAsync();
        ICollection<Throughput> ReadAllThroughputs();
        Throughput CreateThroughput(Throughput throughput);
        void DeleteThroughput(int Id);
        void UpdateThroughput(int Id, Throughput throughput);
    }
}
